import cv2
import numpy as np
from matplotlib import pyplot as plt


src = cv2.imread("w02_p001.png", cv2.IMREAD_GRAYSCALE)
src_col = cv2.imread("w02_p001.png", cv2.IMREAD_COLOR)

edges = cv2.Canny(src, 100, 200)
kernel = np.ones((2,2), np.uint8) 
edges = cv2.dilate(edges, kernel, iterations=2)

lines = cv2.HoughLinesP(edges, 1, np.pi/180, 5, minLineLength = 100, maxLineGap=10)


x_min = 0
for line in lines:
    x1,y1,x2,y2 = line[0]

    if x1 > x_min:
        x_min=x1
        

for line in lines:
    x1,y1,x2,y2 = line[0]

    if abs(y1-y2) < 10 and abs(x1 - x_min) < 150:
        cv2.line(src_col,(x1,y1),(x2,y2),(0,0,255),2)


cv2.imshow('Test image', src_col)
cv2.waitKey(0)
cv2.destroyAllWindows()
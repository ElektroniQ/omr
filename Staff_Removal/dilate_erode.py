import cv2
import numpy as np
from matplotlib import pyplot as plt

def Treshold(image):
    total = src.shape[0] * src.shape[1]
    hist = cv2.calcHist([src],[0], None,[256], [0, 256])/total
    # plt.plot(hist[0:240])
    # plt.yscale('log')
    # plt.show()

    for i in range( 240, -1, -1):
        if hist[i] > 0.0003:
            break

    return i


def GetNotes(image):
    kernel = np.ones((2,2), np.uint8) 
    image = cv2.erode(image, kernel, iterations=3) 
    image = cv2.dilate(image, kernel, iterations=2)
    return image

def GetStaff(image, notes):
    kernel = np.ones((2,2), np.uint8) 
    image = cv2.erode(image, kernel, iterations=1) 
    notes = cv2.dilate(notes, kernel, iterations=3)
    translation_matrix = np.float32([ [1,0,-3], [0,1,-3] ])
    notes = cv2.warpAffine(notes, translation_matrix, (notes.shape[1] , notes.shape[0]))

    return image - notes


src = cv2.imread("w25_p012.png", cv2.IMREAD_GRAYSCALE)

threshold = Treshold(src)

th, raw = cv2.threshold(src, threshold, 255, cv2.THRESH_BINARY_INV)

notes = GetNotes(raw)
staff = GetStaff(raw, notes)

cv2.imshow('Test image', staff)
cv2.waitKey(0)
cv2.destroyAllWindows()
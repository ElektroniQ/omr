import cv2
import numpy as np
from matplotlib import pyplot as plt


src = cv2.imread("w02_p001.png", cv2.IMREAD_GRAYSCALE)

plt.hist(src.ravel(),256,[0,256])
plt.yscale('log')
plt.show()

th, dst = cv2.threshold(src, 235, 255, cv2.THRESH_BINARY_INV)

kernel = np.ones((2,2), np.uint8) 
dst = cv2.erode(dst, kernel, iterations=3) 
dst = cv2.dilate(dst, kernel, iterations=2)

cv2.imshow('Test image', dst)
cv2.waitKey(0)
cv2.destroyAllWindows()